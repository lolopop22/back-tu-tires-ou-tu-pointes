package org.tp.back_3tp_project_rest_service;

import java.util.List;

import org.tp.back_3tp_project_rest_domain.bookings;

public interface BookingService {
	
	public List<bookings> findAllBookings();
	
	public bookings findBooking(Long id);
	
	public bookings addBooking(bookings booking);
	
	public bookings updateBooking(bookings foundBooking);
	
	public void deleteBooking(Long id);
	
	
}
