package org.tp.back_3tp_project_rest_controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tp.back_3tp_project_rest_domain.bookings;
import org.tp.back_3tp_project_rest_service.BookingService;

@RestController
public class BookingController {
	
	@Autowired
	private BookingService bookingService;
	
	@CrossOrigin(origins = "http://localhost:8085")
	@GetMapping("/bookings")
	public List<bookings> findAllBookings(){
		
		return this.bookingService.findAllBookings();
	}
	@CrossOrigin(origins = "http://localhost:8085")
	@GetMapping("/bookings/{id}")
	public bookings findBooking(@PathVariable Long id) {
		
		return this.bookingService.findBooking(id);
	}
	
	@CrossOrigin(origins = "http://localhost:8085")
	@PostMapping("/bookings")
	public bookings addBooking(@RequestBody bookings booking) {
		
		return this.bookingService.addBooking(booking);
	}
	
	@CrossOrigin(origins = "http://localhost:8085")
	@PutMapping("/bookings/{id}")
	bookings updateBooking(@RequestBody bookings newBooking, @PathVariable Long id) {
		
		bookings foundBooking = this.bookingService.findBooking(id);
		
		if(newBooking != null) {
			
			foundBooking.setIdUser(newBooking.getIdUser());
			
			foundBooking.setIdProject(newBooking.getIdProject());
			
			foundBooking.setDuration(newBooking.getDuration());
			
			foundBooking.setBookingPeriod(newBooking.getBookingPeriod());
			
			foundBooking.setBookingStatus(newBooking.getBookingStatus());
			
			foundBooking.setComment(newBooking.getComment());
			
			foundBooking = this.bookingService.updateBooking(foundBooking);
		}
		
		return foundBooking;
	}
	@CrossOrigin(origins = "http://localhost:8085")
	@DeleteMapping("/bookings/{id}")
	public void deleteBooking(@PathVariable Long id) {
		
		this.bookingService.deleteBooking(id);
	}
}
