package org.tp.back_3tp_project_rest_domain;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import lombok.Data;

@Data
@Entity(name = "bookings") // sets the bean's name
@Table(name="bookings")
//@Table(name="BOOKING")
//public class Booking {
public class bookings {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(name = "id_user")
	private int id_user;
	
	@Column(name = "id_project")
	private int id_project;
	
	//private int time;
	@Column(name = "duration")
	private int duration;
	
	//private String period;
	@Column(name = "booking_period")
	private String booking_period;
	
	//private String status;
	@Column(name = "booking_status")
	@Enumerated(EnumType.STRING)
	private booking_status booking_status;
	
	@Column(name = "comment")
	private String comment;
	
	public bookings() {
		
	}
	
	public bookings(int id_user, int id_project, String period, String comment) {
		
		this.id_user = id_user;
		this.id_project = id_project;
		this.duration = 1;
		this.booking_period = period;
		this.booking_status = booking_status.OK;
		this.comment = comment;
		
		
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public int getIdUser() {
		return id_user;
	}
	
	public void setIdUser(int id_user) {
		this.id_user = id_user;
	}
	
	public int getIdProject() {
		return id_project;
	}
	
	public void setIdProject(int id_project) {
		this.id_project = id_project;
	}
	
	public int getDuration() {
		return duration;
	}
	
	public void setDuration(int duration) {
		
		if(duration <= 168) {
			this.duration = duration;
		}
		else {
			System.out.println("Erreur ! Veuillez entrer un nombre d'heures de travail correct ");
		}
		
	}
	
	public String getBookingPeriod() {
		return booking_period;
	}
	
	public void setBookingPeriod(String booking_period) {
		
			Pattern pattern = Pattern.compile("^\\d{4}-\\d{2}");
			Matcher matcher = pattern.matcher(booking_period);
			if(matcher.find() && booking_period.length() == 7) {
				this.booking_period = booking_period;
			}
			else {
				System.out.println("Erreur ! Veuillez choisir une date au format YYYY-MM.");
			}
	}
	
	public booking_status getBookingStatus() {
		return booking_status;
	}
	
	public void setBookingStatus(booking_status booking_status) {
		
		if(booking_status.equals("OK") || booking_status.equals("Rejected") || booking_status.equals("closed")) {
			this.booking_status = booking_status;
		}
		else {
			this.booking_status = booking_status.OK;
		}
	}
	
	public String getComment() {
		return comment;
	}
	
	public void setComment(String comment) {
		this.comment = comment;
	}
}
