package org.tp.back_3tp_project_rest_service_impl;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.tp.back_3tp_project_rest_dao.bookingsRepository;
import org.tp.back_3tp_project_rest_domain.bookings;
import org.tp.back_3tp_project_rest_service.BookingService;
import org.tp.back_3tp_project_rest_domain.*;

@Service
@Transactional
public class BookingServiceImpl implements BookingService {
	
	@Autowired
	private bookingsRepository bookingRepository;
	
	@Override
	@Transactional(readOnly = true)
	public List<bookings> findAllBookings(){
		
		return this.bookingRepository.findAll();
	}
	
	@Override
	@Transactional(readOnly = true)
	public bookings findBooking(Long id) {
		
		return this.bookingRepository.findById(id).orElse(null);
	}
	
	@Override
	public bookings addBooking(bookings booking) {
		/* On ajoute par défaut le status du booking à OK*/
		booking.setBooking_status(booking_status.OK);
		
		/* On met une sécurité pour saisir la period uniquement si elle
		 * respecte le format YYYY-MM */
		Pattern pattern = Pattern.compile("^\\d{4}-\\d{2}");
		Matcher matcher = pattern.matcher(booking.getBookingPeriod());
		
		if(matcher.find() && booking.getBookingPeriod().length() == 7) {
			return this.bookingRepository.save(booking);
		}
		else
		{
			booking.setBookingPeriod("2021-05");
			return this.bookingRepository.save(booking);
		}
		
	}
	
	@Override
	public bookings updateBooking(bookings foundBooking) {
		
		return this.bookingRepository.save(foundBooking);
	}
	
	@Override
	public void deleteBooking(Long id) {
		
		this.bookingRepository.deleteById(id);
	}

}
