/*package org.tp.back_3tp_project_rest_database;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tp.back_3tp_project_rest_dao.BookingRepository;
import org.tp.back_3tp_project_rest_domain.Booking;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class LoadDatabase {

	@Bean
	CommandLineRunner initDatabase(BookingRepository bookingRepository) {
		return args -> {
			
			log.info("Preloading " + bookingRepository.save(new Booking(1,1,"J'ai travaillé sur le projet A")));
			
			log.info("Preloading " + bookingRepository.save(new Booking(2,1,"J'ai aussi travaillé sur le projet A")));
			
		};
	}
}*/
