package org.tp.back_3tp_project_rest_dao;

import org.springframework.data.jpa.repository.JpaRepository;

import org.tp.back_3tp_project_rest_domain.*;

public interface bookingsRepository extends JpaRepository<bookings, Long>{	

}
