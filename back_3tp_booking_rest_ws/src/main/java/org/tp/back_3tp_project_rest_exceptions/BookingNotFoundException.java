package org.tp.back_3tp_project_rest_exceptions;

public class BookingNotFoundException extends RuntimeException {
	
	BookingNotFoundException(Long id){
		super("Could not find booking" + id);
	}
}
