package org.tp;

import org.h2.server.web.WebServlet;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;

/*@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class}) */
@SpringBootApplication
public class Back3tpProjectRestWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Back3tpProjectRestWsApplication.class, args);
	}
	
}

