/*package org.tp.back_3tp_project_rest_ws;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.tp.back_3tp_project_rest_dao.BookingRepository;
import org.tp.back_3tp_project_rest_domain.Booking;

import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class BookingControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private BookingRepository bookingRepository;
	/*
	@Test
	public void testAllBookings() throws Exception{
		mockMvc.perform(
				MockMvcRequestBuilders
				.get("/booking")
				.accept(MediaType.APPLICATION_JSON)
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.length()", is(2)))
		);
				
	}
	
	@Test
	public void testOneBooking() throws Exception{
		mockMvc.perform(
				MockMvcRequestBuilders
				.get("/booking/1")
				.accept(MediaType.APPLICATION_JSON)
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.id_user", is(1)))
				.andExpect(jsonPath("$.id_project", is(1)))
				.andExpect(jsonPath("$.comment", is("J'ai travaillé sur le projet A")))
		);
				
	}
	
	@Test
	public void testCreateBooking() throws Exception{
		
		Booking booking = new Booking(3,2,"Moi je travaille sur le projet B.");
		
		ObjectMapper mapper = new ObjectMapper();
		byte[] bookingAsBytes = mapper.writeValueAsBytes(booking);
		
		mockMvc.perform(
				MockMvcRequestBuilders
				.post("/booking")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON).content(bookingAsBytes))
				.andExpect(status().isOk());
		
		assertEquals(3, bookingRepository.count());
		
		Collection<Booking> bookings = bookingRepository.findAll();
		
		boolean found = false;
		
		for(Booking bookingActuel : bookings) {
			
			if(bookingActuel.getIdUser() == 3 && bookingActuel.getIdProject() == 2 && bookingActuel.getComment().equals("Moi je travaille sur le projet B.")) {
				
				found = true;
				
				bookingRepository.delete(bookingActuel);
			}
		}
		
		assertTrue(found);
	}
	
	@Test
	public void testDeleteBooking() throws Exception{
		
		Booking booking = new Booking(3,2,"Moi je travaille sur le projet B.");
		
		bookingRepository.save(booking);
		
		Collection<Booking> bookings = bookingRepository.findAll();
		
		Long id = 5L;
		
		for(Booking bookingActuel : bookings) {
			
			if(bookingActuel.getIdUser() == 3 && bookingActuel.getIdProject() == 2 && bookingActuel.getComment().equals("Moi je travaille sur le projet B.")) {
				
				id = bookingActuel.getId();
			}
		}
		
		mockMvc.perform(
				MockMvcRequestBuilders
				.delete("/booking/" + id)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
		
		assertEquals(2, bookingRepository.count());
		
	}
	
	@Test
	public void testUpdateBooking() throws Exception{
		
		Booking booking = new Booking(3,2,"Moi je travaille sur le projet B.");
		
		ObjectMapper mapper = new ObjectMapper();
		byte[] bookingAsBytes = mapper.writeValueAsBytes(booking);
		
		mockMvc.perform(
				MockMvcRequestBuilders
				.put("/booking/3")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON).content(bookingAsBytes))
				.andExpect(status().isOk());
		
		booking = bookingRepository.findById(3L).orElse(null);
		
		if(booking == null) {
			
			fail("Booking not found");
		}
	}
	*/
	/*
}*/
