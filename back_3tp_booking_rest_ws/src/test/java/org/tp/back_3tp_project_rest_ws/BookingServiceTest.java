/*package org.tp.back_3tp_project_rest_ws;

import java.util.Collection;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.tp.back_3tp_project_rest_dao.bookingsRepository;
import org.tp.back_3tp_project_rest_domain.bookings;
import org.tp.back_3tp_project_rest_domain.booking_status;
import org.tp.back_3tp_project_rest_service.BookingService;

@SpringBootTest
@ActiveProfiles("test")
public class BookingServiceTest {
	
	@Autowired
	private BookingService bookingService;
	
	@Autowired
	private bookingsRepository bookingRepository;
	
	@Test
	public void testFindAllBookings() throws Exception{
		
		Collection<bookings> allBookings = this.bookingService.findAllBookings();
		
		Assertions.assertEquals(2, allBookings.size());
	}
	
	@Test
	public void testFindBooking() throws Exception{
		
		bookings booking = this.bookingService.findBooking(1L);
		
		Assertions.assertEquals(1, booking.getIdUser());
		Assertions.assertEquals(1, booking.getIdProject());
		Assertions.assertEquals(1, booking.getDuration());
		Assertions.assertEquals(2020-05-04, booking.getBookingPeriod());
		Assertions.assertEquals(booking_status.OK, booking.getBookingStatus());
		//Assertions.assertEquals("J'ai travaillé sur le projet A",booking.getComment());
	}
	
	@Test
	public void testCreateBooking() throws Exception{
		
		bookings booking = new bookings();
		booking.setIdUser(2);
		booking.setIdProject(2);
		booking.setComment("Moi je travaille sur le projet B.");
		
		booking = this.bookingService.addBooking(booking);
		
		Collection<bookings> allBookings = this.bookingService.findAllBookings();
		
		Assertions.assertEquals(3, allBookings.size());
		
		this.bookingRepository.delete(booking);
	}
	
	@Test
	public void testDeleteBooking() throws Exception{
		
		this.bookingService.deleteBooking(1L);
		
		Collection<bookings> allBookings = this.bookingService.findAllBookings();
		
		Assertions.assertEquals(1, allBookings.size());
		
		bookings booking = new bookings();
		booking.setIdUser(1);
		booking.setIdProject(1);
		booking.setComment("J'ai travaillé sur le projet A");
	}
	
	@Test
	public void testUpdateBooking() throws Exception{
		
		bookings booking = this.bookingRepository.findById(1L).orElse(null);
		
		booking.setIdProject(2);
		booking.setTime(2);
		booking.setPeriod("Février");
		booking.setComment("A présent je travaille sur le projet B.");
		
		this.bookingService.updateBooking(booking);
		
		booking = this.bookingService.findBooking(1L);
		
		Assertions.assertEquals(1, booking.getIdUser());
		Assertions.assertEquals(2, booking.getIdProject());
		Assertions.assertEquals(2, booking.getTime());
		Assertions.assertEquals("Février", booking.getPeriod());
		Assertions.assertEquals("Ok", booking.getStatus());
		Assertions.assertEquals("A présent je travaille sur le projet B.",booking.getComment());
		
		booking.setIdProject(1);
		booking.setTime(1);
		booking.setPeriod("Janvier");
		booking.setComment("J'ai travaillé sur le projet A");
		
		this.bookingRepository.save(booking);
	}
}*/
