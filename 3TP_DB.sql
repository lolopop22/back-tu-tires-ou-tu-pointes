CREATE TABLE USERS
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    user_name VARCHAR(20) NOT NULL,
    id_manager INT DEFAULT 0,
    pass CHAR(32) NOT NULL,
    role ENUM('DEV', 'MANAGER') DEFAULT "DEV",
    Active Boolean DEFAULT 1
    /*FOREIGN KEY (id_manager) REFERENCES USERS(id)*/
);

CREATE TABLE PROJECTS
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    technical_name VARCHAR(50) NOT NULL,
    functional_name VARCHAR(50),
    project_status ENUM('Open', 'Closed') DEFAULT "Open"
);

CREATE TABLE BOOKINGS
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    id_user INT NOT NULL,
    id_project INT NOT NULL,
    duration INT DEFAULT 0,
    booking_period VARCHAR(7) NOT NULL,
    booking_status ENUM('OK', 'Rejected', 'closed') DEFAULT "OK",
    comment VARCHAR(100),
    FOREIGN KEY (id_user) REFERENCES USERS(id),
    FOREIGN KEY (id_project) REFERENCES PROJECTS(id)
);

/*USERS*/

INSERT INTO `USERS` (`first_name`, `last_name`, `user_name`, `id_manager`, `pass`, `role`, `active`) VALUES
('loic', 'assontia', 'assontia', 0, MD5('test1'), 'MANAGER', 1);
INSERT INTO `USERS` (`first_name`, `last_name`, `user_name`, `id_manager`, `pass`, `active`) VALUES
('remi', 'vanelle', 'vanelle', 1, MD5('test2'), 1);

/*PROJECTS*/
INSERT INTO `PROJECTS`(`technical_name`, `functional_name`) VALUES ("Project 1", "Application_1");
INSERT INTO `PROJECTS`(`technical_name`, `functional_name`) VALUES ("Project 2", "Application_2");

/*BOOKINGS*/
INSERT INTO `BOOKINGS`(`id_user`, `id_project`, `duration`, `booking_period`) VALUES (1,1,1,"2020-05");
INSERT INTO `BOOKINGS`(`id_user`, `id_project`, `duration`, `booking_period`) VALUES (1,2,1,"2020-05");
