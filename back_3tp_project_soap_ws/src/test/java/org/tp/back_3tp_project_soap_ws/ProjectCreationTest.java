package org.tp.back_3tp_project_soap_ws;

import static org.springframework.ws.test.server.RequestCreators.withPayload;
import static org.springframework.ws.test.server.ResponseMatchers.payload;

import javax.xml.transform.Source;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.ws.test.server.MockWebServiceClient;
import org.springframework.xml.transform.StringSource;

@SpringBootTest
public class ProjectCreationTest {

	@Autowired
	private ApplicationContext applicationContext;
	
	private MockWebServiceClient mockWebServiceClient;
	
	@BeforeEach
	public void createClient() throws Exception {
		mockWebServiceClient = MockWebServiceClient.createClient(applicationContext);
	}
	
	@Test
	public void testCreateProject() throws Exception{
		
		Source requestPayload = new StringSource(
			"<pro:createProjectRequest xmlns:pro='http://spring.wsProject.com/project'>"
				+ "	<pro:technicalName>Project 3</pro:technicalName>"
				+ " <pro:functionalName>Application_3</pro:functionalName>"
				+ "</pro:createProjectRequest>");
		
		Source expectedResponsePayload = new StringSource(
				"<ns2:createProjectResponse"
				+ "            xmlns:ns2=\"http://spring.wsProject.com/project\">"
				+ "            <ns2:project>"
				+ "                <ns2:projectId>5</ns2:projectId>"
				+ "                <ns2:technicalName>Project 3</ns2:technicalName>"
				+ "                <ns2:functionalName>Application_3</ns2:functionalName>"
				+ "                <ns2:projectStatus>Open</ns2:projectStatus>"
				+ "            </ns2:project>"
				+ "        </ns2:createProjectResponse>");
		
		mockWebServiceClient
		.sendRequest(withPayload(requestPayload))
		.andExpect(payload(expectedResponsePayload));
	}
	
	@Test
	public void testCreateAlreadyExistingProject() throws Exception{
		
		Source requestPayload = new StringSource(
			"<pro:createProjectRequest xmlns:pro='http://spring.wsProject.com/project'>"
				+ "	<pro:technicalName>Project 1</pro:technicalName>"
				+ " <pro:functionalName>Application_1</pro:functionalName>"
				+ "</pro:createProjectRequest>");
		
		Source expectedResponsePayload = new StringSource(
				"<SOAP-ENV:Fault xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
				+ "            <faultcode>SOAP-ENV:Server</faultcode>\n"
				+ "            <faultstring xml:lang=\"en\">The project named Project 1 already exists.</faultstring>\n"
				+ "        </SOAP-ENV:Fault>");
		
		mockWebServiceClient
		.sendRequest(withPayload(requestPayload))
		.andExpect(payload(expectedResponsePayload));
	}
}
