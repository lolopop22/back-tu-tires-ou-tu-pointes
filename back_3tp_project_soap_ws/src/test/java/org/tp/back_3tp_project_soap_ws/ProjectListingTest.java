package org.tp.back_3tp_project_soap_ws;

import static org.springframework.ws.test.server.RequestCreators.withPayload;
import static org.springframework.ws.test.server.ResponseMatchers.payload;

import javax.xml.transform.Source;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.ws.test.server.MockWebServiceClient;
import org.springframework.xml.transform.StringSource;

@SpringBootTest
public class ProjectListingTest {
	
	@Autowired
	private ApplicationContext applicationContext;
	
	private MockWebServiceClient mockWebServiceClient;
	
	@BeforeEach
	public void createClient() throws Exception {
		mockWebServiceClient = MockWebServiceClient.createClient(applicationContext);
	}
	
	@Test
	public void testListProject() throws Exception{
		
		Source requestPayload = new StringSource(
				"<pro:getAllProjectsRequest xmlns:pro='http://spring.wsProject.com/project'>"
				+ "</pro:getAllProjectsRequest>");
		
		Source expectedResponsePayload = new StringSource(
				"<ns2:getAllProjectsResponse xmlns:ns2='http://spring.wsProject.com/project'>"
				+ "            <ns2:project>"
				+ "                <ns2:projectId>1</ns2:projectId>"
				+ "                <ns2:technicalName>Project 1</ns2:technicalName>"
				+ "                <ns2:functionalName>Application_1</ns2:functionalName>"
				+ "                <ns2:projectStatus>Open</ns2:projectStatus>"
				+ "            </ns2:project>"
				+ "            <ns2:project>"
				+ "                <ns2:projectId>2</ns2:projectId>"
				+ "                <ns2:technicalName>Project 2</ns2:technicalName>"
				+ "                <ns2:functionalName>Application_2</ns2:functionalName>"
				+ "                <ns2:projectStatus>Open</ns2:projectStatus>"
				+ "            </ns2:project>"
				+ "        </ns2:getAllProjectsResponse>");
		
		mockWebServiceClient
		.sendRequest(withPayload(requestPayload))
		.andExpect(payload(expectedResponsePayload));
	}

}
