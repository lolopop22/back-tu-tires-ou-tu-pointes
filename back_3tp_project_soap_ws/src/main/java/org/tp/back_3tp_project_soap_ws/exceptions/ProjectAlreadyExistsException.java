package org.tp.back_3tp_project_soap_ws.exceptions;

public class ProjectAlreadyExistsException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	public ProjectAlreadyExistsException(String technicalName) {
		super("The project named " + technicalName +  " already exists.");
	}


}
