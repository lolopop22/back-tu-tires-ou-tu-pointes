package org.tp.back_3tp_project_soap_ws.endpoints;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.tp.back_3tp_project_soap_ws.exceptions.ProjectAlreadyExistsException;
import org.tp.back_3tp_project_soap_ws.services.ProjectService;

import com.wsproject.spring.project.CreateProjectRequest;
import com.wsproject.spring.project.CreateProjectResponse;
import com.wsproject.spring.project.GetAllProjectsResponse;
import com.wsproject.spring.project.ProjectInfo;


@Endpoint
public class ProjectEndpoints {
	
	private static final String NAMESPACE_URI = "http://spring.wsProject.com/project";
	
	@Autowired
	private ProjectService projectService;
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllProjectsRequest")
	@ResponsePayload
	public GetAllProjectsResponse getAllProjects(){
		
		GetAllProjectsResponse response = new GetAllProjectsResponse();
				
		List<ProjectInfo> projects = this.projectService.findAllProjects();

		if(projects != null) {
			for(ProjectInfo project : projects) {
				//System.out.println(project.getFunctionalName());
				response.getProject().add(project);
			}
		}
		
		return response;
		
	}
	
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "createProjectRequest")
	@ResponsePayload
	public CreateProjectResponse createUser(@RequestPayload CreateProjectRequest request) throws ProjectAlreadyExistsException{
		
		CreateProjectResponse response = new CreateProjectResponse();
		
		String technicalName = request.getTechnicalName();
		String functionalName = request.getFunctionalName();
		
		response.setProject(this.projectService.createProject(technicalName, functionalName));
		
		return response;
	}
	
}
