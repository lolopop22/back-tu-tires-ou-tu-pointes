package org.tp.back_3tp_project_soap_ws.conversion;

import org.springframework.stereotype.Component;
import org.tp.back_3tp_project_soap_ws.domains.ProjectEntity;

import com.wsproject.spring.project.ProjectInfo;

@Component
public class ProjectConvertor {

	public ProjectInfo convertToSoapFormat(ProjectEntity projectEntity) {
		
		ProjectInfo projectSoap = new ProjectInfo();
		
		projectSoap.setProjectId((int)(long)projectEntity.getId());
		projectSoap.setFunctionalName(projectEntity.getFunctionalName());
		projectSoap.setTechnicalName(projectEntity.getTechnicalName());
		projectSoap.setProjectStatus(projectEntity.getProjectStatus().name());

		return projectSoap;
	}

}
