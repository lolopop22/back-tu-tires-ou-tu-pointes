package org.tp.back_3tp_project_soap_ws.domains;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;


@Data
@Entity(name = "PROJECTS") // sets the bean's name
@Table(name="PROJECTS")
public class ProjectEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "technical_name")
	private String technicalName;
	
	
	@Column(name = "functional_name")
	private String functionalName;
	
	@Column(name = "project_status")
	@Enumerated(EnumType.STRING)
	private ProjectStatus projectStatus;
	
	public ProjectEntity() {
		
	}
	
	public ProjectEntity(String technicalName, String functionalName) {
		this.technicalName = technicalName;
		this.functionalName = functionalName;
		this.projectStatus = ProjectStatus.valueOf("Open");
	}
	
}
