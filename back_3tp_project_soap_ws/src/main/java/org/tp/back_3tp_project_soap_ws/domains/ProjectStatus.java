package org.tp.back_3tp_project_soap_ws.domains;

public enum ProjectStatus {
	Open,
	Closed
}
