package org.tp.back_3tp_project_soap_ws.services;

import java.util.List;

import org.tp.back_3tp_project_soap_ws.exceptions.ProjectAlreadyExistsException;

import com.wsproject.spring.project.ProjectInfo;

public interface ProjectService {

	public List<ProjectInfo> findAllProjects();

	public ProjectInfo createProject(String technicalName, String functionalName) throws ProjectAlreadyExistsException;

}
