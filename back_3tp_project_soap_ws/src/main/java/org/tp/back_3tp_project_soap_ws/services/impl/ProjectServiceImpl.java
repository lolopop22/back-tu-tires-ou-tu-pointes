package org.tp.back_3tp_project_soap_ws.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.tp.back_3tp_project_soap_ws.conversion.ProjectConvertor;
import org.tp.back_3tp_project_soap_ws.dao.ProjectRepository;
import org.tp.back_3tp_project_soap_ws.domains.ProjectEntity;
import org.tp.back_3tp_project_soap_ws.exceptions.ProjectAlreadyExistsException;
import org.tp.back_3tp_project_soap_ws.services.ProjectService;

import com.wsproject.spring.project.ProjectInfo;

@Service
@Transactional
public class ProjectServiceImpl implements ProjectService{

	@Autowired
	private ProjectRepository projectRepository;
	
	@Autowired
	private ProjectConvertor projectConvertor;

	@Override
	public List<ProjectInfo> findAllProjects() {
		
		System.out.println("in findAllProjects | coucou");
		
		List<ProjectEntity> projectEntityList = this.projectRepository.findAll();
		
		System.out.println("in findAllProjects | re coucou");
		
		List<ProjectInfo> projectSoapList = new ArrayList<ProjectInfo>();
		
		if(!projectEntityList.isEmpty()) {
			for(ProjectEntity projectEntity: projectEntityList) {
				/*System.out.println("in findAllProjects | re re coucou");
				System.out.println(projectEntity.getFunctionalName());*/
				projectSoapList.add(this.projectConvertor.convertToSoapFormat(projectEntity));
			}
			return projectSoapList;
		}
		
		return null;
		
	}
	
	
	public ProjectInfo createProject(String technicalName, String functionalName) throws ProjectAlreadyExistsException{
		
		Optional<ProjectEntity> alreadyExistingProject = this.projectRepository.findByTechnicalName(technicalName);
			
		//alreadyExistingProject.orElseThrow(() -> new ProjectAlreadyExistsException(technicalName));
			
		if (!alreadyExistingProject.isPresent()) {
			ProjectEntity newProject = new ProjectEntity(technicalName, functionalName);
			
			ProjectEntity newSavedProject = this.projectRepository.save(newProject);
			
			return this.projectConvertor.convertToSoapFormat(newSavedProject);
		} else {
			
			throw new ProjectAlreadyExistsException(technicalName);
			
		}
				
	}

}
