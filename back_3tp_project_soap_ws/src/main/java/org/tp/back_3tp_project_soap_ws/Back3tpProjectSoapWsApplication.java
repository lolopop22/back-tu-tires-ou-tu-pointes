package org.tp.back_3tp_project_soap_ws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Back3tpProjectSoapWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Back3tpProjectSoapWsApplication.class, args);
	}

}
