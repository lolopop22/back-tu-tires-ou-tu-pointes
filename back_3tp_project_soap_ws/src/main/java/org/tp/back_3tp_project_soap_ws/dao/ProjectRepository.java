package org.tp.back_3tp_project_soap_ws.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.tp.back_3tp_project_soap_ws.domains.ProjectEntity;

public interface ProjectRepository extends JpaRepository<ProjectEntity, Long>{

	Optional<ProjectEntity> findByTechnicalName(String technicalName);

}
