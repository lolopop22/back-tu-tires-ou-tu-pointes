package org.tp.back_3tp_user_soap_ws;

import static org.springframework.ws.test.server.RequestCreators.*;
import static org.springframework.ws.test.server.ResponseMatchers.*;

import javax.xml.transform.Source;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.ws.test.server.MockWebServiceClient;
import org.springframework.xml.transform.StringSource;

@SpringBootTest
public class UserLoginTest {
	
	@Autowired
	private ApplicationContext applicationContext;
	
	private MockWebServiceClient mockWebServiceClient;
	
	@BeforeEach
	public void createClient() throws Exception {
		mockWebServiceClient = MockWebServiceClient.createClient(applicationContext);
	}
	
	@Test
	public void testGoodUserLogin() throws Exception{
		Source requestPayload = new StringSource(
				"<use:loginUserRequest xmlns:use='http://spring.wsProject.com/users'>"
						+ " 	<use:userCredentials>"
						+ " 		<use:userName>assontia</use:userName>"
						+ "         <use:password>test1</use:password>"
						+ "     </use:userCredentials>"
						+ " </use:loginUserRequest>");
		
		Source expectedResponsePayload = new StringSource(
				"<ns2:loginUserResponse"
				+ "            xmlns:ns2=\"http://spring.wsProject.com/users\">"
				+ "            <ns2:loginStatus>true</ns2:loginStatus>"
				+ "            <ns2:message>User with username assontia successfully logged in.</ns2:message>"
				+ "            <ns2:user>"
				+ "                <ns2:userId>1</ns2:userId>"
				+ "                <ns2:managerId>0</ns2:managerId>"
				+ "                <ns2:firstName>loic</ns2:firstName>"
				+ "                <ns2:lastName>assontia</ns2:lastName>"
				+ "                <ns2:userName>assontia</ns2:userName>"
				+ "                <ns2:userRole>MANAGER</ns2:userRole>"
				+ "                <ns2:active>true</ns2:active>"
				+ "            </ns2:user>"
				+ "        </ns2:loginUserResponse>");
		
		
		mockWebServiceClient
			.sendRequest(withPayload(requestPayload))
			.andExpect(payload(expectedResponsePayload));
	}
	
	@Test
	public void testUserLoginBadUsername() throws Exception{
		Source requestPayload = new StringSource(
				"<use:loginUserRequest xmlns:use='http://spring.wsProject.com/users'>"
						+ " 	<use:userCredentials>"
						+ " 		<use:userName>toto</use:userName>"
						+ "         <use:password>test1</use:password>"
						+ "     </use:userCredentials>"
						+ " </use:loginUserRequest>");
		
		Source expectedResponsePayload = new StringSource(
				"<ns2:loginUserResponse xmlns:ns2='http://spring.wsProject.com/users'>"
						+ "	<ns2:loginStatus>false</ns2:loginStatus>"
						+ " <ns2:message>User with username toto couldn't log in.</ns2:message>"
						+ "</ns2:loginUserResponse>");
		
		mockWebServiceClient
			.sendRequest(withPayload(requestPayload))
			.andExpect(payload(expectedResponsePayload));
	}
	
	@Test
	public void testUserLoginBadPassword() throws Exception{
		Source requestPayload = new StringSource(
				"<use:loginUserRequest xmlns:use='http://spring.wsProject.com/users'>"
						+ " 	<use:userCredentials>"
						+ " 		<use:userName>assontia</use:userName>"
						+ "         <use:password>badpassword</use:password>"
						+ "     </use:userCredentials>"
						+ " </use:loginUserRequest>");
		
		Source expectedResponsePayload = new StringSource(
				"<ns2:loginUserResponse xmlns:ns2='http://spring.wsProject.com/users'>"
						+ "	<ns2:loginStatus>false</ns2:loginStatus>"
						+ " <ns2:message>User with username assontia couldn't log in.</ns2:message>"
						+ "</ns2:loginUserResponse>");
		
		mockWebServiceClient
			.sendRequest(withPayload(requestPayload))
			.andExpect(payload(expectedResponsePayload));
	}
	
}
