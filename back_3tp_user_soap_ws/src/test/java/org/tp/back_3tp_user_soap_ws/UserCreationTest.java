package org.tp.back_3tp_user_soap_ws;

import static org.springframework.ws.test.server.RequestCreators.*;
import static org.springframework.ws.test.server.ResponseMatchers.*;

import javax.xml.transform.Source;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.ws.test.server.MockWebServiceClient;
import org.springframework.xml.transform.StringSource;

@SpringBootTest
public class UserCreationTest {
	
	@Autowired
	private ApplicationContext applicationContext;
	
	private MockWebServiceClient mockWebServiceClient;
	
	@BeforeEach
	public void createClient() throws Exception {
		mockWebServiceClient = MockWebServiceClient.createClient(applicationContext);
	}
	
	@Test
	public void testCreateUser() throws Exception{
		Source requestPayload = new StringSource(
				"<use:createUserRequest xmlns:use='http://spring.wsProject.com/users'>"
				+ "	<use:managerId>1</use:managerId>"
				+ " <use:firstName>Alexandre</use:firstName>"
				+ "	<use:lastName>LASSIAZ</use:lastName>"
				+ " <use:userRole>DEV</use:userRole>"
				+ "</use:createUserRequest>");
		
		Source expectedResponsePayload = new StringSource(
				"<ns2:createUserResponse xmlns:ns2=\"http://spring.wsProject.com/users\">"
				+ " <ns2:user>"
				+ "		<ns2:userId>3</ns2:userId>"
				+ "     <ns2:managerId>1</ns2:managerId>"
				+ "     <ns2:firstName>Alexandre</ns2:firstName>"
				+ "     <ns2:lastName>LASSIAZ</ns2:lastName>"
				+ "     <ns2:userName>alexandre</ns2:userName>"
				+ "     <ns2:userRole>DEV</ns2:userRole>"
				+ "     <ns2:active>true</ns2:active>"
				+ "     </ns2:user>"
				+ "</ns2:createUserResponse>");
		
		mockWebServiceClient
		.sendRequest(withPayload(requestPayload))
		.andExpect(payload(expectedResponsePayload));
	}

}
