package org.tp.back_3tp_user_soap_ws.exceptions;

public class UsersNotFoundException extends RuntimeException{
	
	private static final long serialVersionUID = 1L;
	
	public UsersNotFoundException(Long id) {
			
		super("The manager with id " + id + " does not have developpers under his command.");
			
	}

}
