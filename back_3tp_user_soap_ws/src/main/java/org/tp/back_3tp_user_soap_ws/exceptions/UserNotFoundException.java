package org.tp.back_3tp_user_soap_ws.exceptions;

public class UserNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public UserNotFoundException(Long id) {
			
		super("Could not find the user with id " + id);
			
	}
	
	public UserNotFoundException(String userName) {
		
		super("Could not find the user with username " + userName);
			
	}

}
