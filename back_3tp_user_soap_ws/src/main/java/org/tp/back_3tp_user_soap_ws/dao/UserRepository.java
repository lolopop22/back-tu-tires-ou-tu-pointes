package org.tp.back_3tp_user_soap_ws.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.tp.back_3tp_user_soap_ws.domains.UserEntity;


public interface UserRepository extends JpaRepository<UserEntity, Long> {
	
	Optional<UserEntity> findByUserName(String userName);
	
/*	@Query(value = "select u.id, u.first_name, u.last_name, u.user_name, u.id_manager, u.role, u.active from USERS u where u.id_manager = :managerId", nativeQuery = true)*/
	@Query(value = "select * from USERS u where u.id_manager = :managerId", nativeQuery = true)

	Optional<List<UserEntity>> findUsersByManager(@Param("managerId") Long managerId);

}
