package org.tp.back_3tp_user_soap_ws.services;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.tp.back_3tp_user_soap_ws.exceptions.UserNotFoundException;
import org.tp.back_3tp_user_soap_ws.domains.UserEntity;

import com.wsproject.spring.users.UserInfo;

public interface UserService {

	UserEntity loadUserByUserLogin(String userName) throws UserNotFoundException;

	Boolean isValid(String userName, String password) throws NoSuchAlgorithmException;

	public UserInfo findUser(Long userId);

	public List<UserInfo> findAllUsers();

	public List<UserInfo> findUsersByManager(Long managerId);

	public UserInfo createUser(String firstName, String lastName, String userRole, int managerId) throws NoSuchAlgorithmException;

	public UserInfo findUserByUsername(String userName) throws UserNotFoundException;

}
