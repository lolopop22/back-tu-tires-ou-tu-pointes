package org.tp.back_3tp_user_soap_ws.services.impl;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.tp.back_3tp_user_soap_ws.conversion.UserConvertor;
import org.tp.back_3tp_user_soap_ws.dao.UserRepository;
import org.tp.back_3tp_user_soap_ws.domains.UserEntity;
import org.tp.back_3tp_user_soap_ws.exceptions.UserNotFoundException;
import org.tp.back_3tp_user_soap_ws.exceptions.UsersNotFoundException;
import org.tp.back_3tp_user_soap_ws.services.UserService;

import com.wsproject.spring.users.UserInfo;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserConvertor userConvertor;
	
	@Override
	public UserEntity loadUserByUserLogin(String userName) throws UserNotFoundException {
		
		//optional utilisé dans le cas où on ne trouve pas l'utilisateur 
		try {
			Optional<UserEntity> userEntity = userRepository.findByUserName(userName);
			
			userEntity.orElseThrow(() -> new UserNotFoundException(userName));
			
			if (userEntity.isPresent()) {
				return userEntity.get();
			}
		} catch(UserNotFoundException e) {
			System.out.println(e.toString());
		}
		
		return null;
        
	}
	
	@Override
	public UserInfo findUserByUsername(String userName) throws UserNotFoundException {
		
		//optional utilisé dans le cas où on ne trouve pas l'utilisateur 
		try {
			Optional<UserEntity> userEntity = userRepository.findByUserName(userName);
			
			userEntity.orElseThrow(() -> new UserNotFoundException(userName));
			
			if (userEntity.isPresent()) {
				UserInfo userInfo = this.userConvertor.convertToSoapFormat(userEntity.get());
				return userInfo;
			}
		} catch(UserNotFoundException e) {
			System.out.println(e.toString());
		}
		
		return null;
        
	}
	
	@Override
	public Boolean isValid(String userName, String password) throws NoSuchAlgorithmException {
		
		UserEntity user = this.loadUserByUserLogin(userName);
		
		if(user != null) {
			// On chiffre le mot de passe en utilisant l'algo de hachage MD5
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(password.getBytes());
	        byte[] digest = md.digest();
	        StringBuilder sb = new StringBuilder(32);
	        for (byte b : digest) {
	            sb.append(String.format("%02x", b & 0xff));
	        }
	        System.out.println(password);
	        System.out.println(sb.toString());
	        System.out.println(user.getPassword());
			if(!user.getPassword().equals(sb.toString())) {
				
				System.out.println("userName " + userName + " valide mais Mot de passe invalide!");
				return false;
			} else {
				System.out.println("userName " + userName + " valide et mot de passe valide!");
				return true;
			}
		} else {
			System.out.println("userName " + userName + " invalide");
			return false;
		}
	}
	
	
	@Override
	@Transactional(readOnly = true)
	public UserInfo findUser(Long userId) {
		
		UserEntity userEntity = this.userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
		
		System.out.println("user found in user sevice impl: " + userEntity);
		
		if (userEntity != null) {
			UserInfo userInfo = this.userConvertor.convertToSoapFormat(userEntity);
			return userInfo;
		}
		
		return null;
	}
	
	@Override
	public List<UserInfo> findAllUsers(){
		
		List<UserEntity> userEntityList = this.userRepository.findAll();
		
		List<UserInfo> userSoapList = new ArrayList<UserInfo>();
		
		if(!userEntityList.isEmpty()) {
			for(UserEntity userEntity: userEntityList) {
				userSoapList.add(this.userConvertor.convertToSoapFormat(userEntity));
			}
			return userSoapList;
		}
		
		return null;
		
	}
	
	@Override
	public List<UserInfo> findUsersByManager(Long managerId){
		
		try {
			
			Optional<List<UserEntity>> userEntityListByManager = this.userRepository.findUsersByManager(managerId);
			
			userEntityListByManager.orElseThrow(() -> new UsersNotFoundException(managerId));
			
			if (userEntityListByManager.isPresent()) {
				
				List<UserEntity> users = userEntityListByManager.get();
				
				List<UserInfo> userSoapListByManager = new ArrayList<UserInfo>();
				
				if(!users.isEmpty()) {
					for(UserEntity userEntity: users) {
						userSoapListByManager.add(this.userConvertor.convertToSoapFormat(userEntity));
					}
					return userSoapListByManager;
				}
			} 
			
		} catch(UsersNotFoundException e) {
			System.out.println(e.toString());
		}
		
		return null;
		
	}
	
	@Override
	public UserInfo createUser(String firstName, String lastName, String userRole, int managerId) throws NoSuchAlgorithmException {
		
		// UserInfo manager = this.findUser(Long.valueOf(managerId));
		
		String userName = firstName.toLowerCase();
		
		System.out.println("The manager exists. We can proceed.");
		
		/* construction d'un mot de passe par défaut*/
		String defaultPassword = "default_password";
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(defaultPassword.getBytes());
        byte[] digest = md.digest();
        StringBuilder sb = new StringBuilder(32);
        for (byte b : digest) {
            sb.append(String.format("%02x", b & 0xff));
        }
		
		UserEntity newUser = new UserEntity(firstName, lastName, userName, managerId, userRole, sb.toString());
		
		UserEntity newUserSaved = this.userRepository.save(newUser);
		
		return this.userConvertor.convertToSoapFormat(newUserSaved);
		
		/*if (manager != null) {
			
			System.out.println("The manager exists. We can proceed.");
			
			UserEntity newUser = new UserEntity(firstName, lastName, userName, managerId, userRole);
			
			UserEntity newUserSaved = this.userRepository.save(newUser);
			
			return this.userConvertor.convertToSoapFormat(newUserSaved);
			
		} else {
			System.out.println("The manager doesn't exist. Process failed.");
			return null;
		}*/
		
		
	}
}
