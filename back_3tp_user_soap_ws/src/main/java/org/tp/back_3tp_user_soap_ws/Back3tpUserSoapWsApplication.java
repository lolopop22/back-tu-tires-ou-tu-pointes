package org.tp.back_3tp_user_soap_ws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Back3tpUserSoapWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Back3tpUserSoapWsApplication.class, args);
	}

}
