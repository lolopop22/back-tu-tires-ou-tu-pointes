package org.tp.back_3tp_user_soap_ws.endpoints;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.tp.back_3tp_user_soap_ws.services.UserService;

import com.wsproject.spring.users.CreateUserRequest;
import com.wsproject.spring.users.CreateUserResponse;
import com.wsproject.spring.users.GetAllUsersResponse;
import com.wsproject.spring.users.GetUserRequest;
import com.wsproject.spring.users.GetUserResponse;
import com.wsproject.spring.users.GetUsersByManagerRequest;
import com.wsproject.spring.users.GetUsersByManagerResponse;
import com.wsproject.spring.users.LoginUserRequest;
import com.wsproject.spring.users.LoginUserResponse;
import com.wsproject.spring.users.UserInfo;


@Endpoint
public class UserEndpoints {
	
	private static final String NAMESPACE_URI = "http://spring.wsProject.com/users";
	
	@Autowired
	private UserService userService;
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "loginUserRequest")
	@ResponsePayload
	public LoginUserResponse loginUser(@RequestPayload LoginUserRequest request) throws NoSuchAlgorithmException {
		
		LoginUserResponse response = new LoginUserResponse();
		String userName = request.getUserCredentials().getUserName();
		String password = request.getUserCredentials().getPassword();
		
		Boolean isValid = this.userService.isValid(userName, password);
		
		if(isValid){
			UserInfo foundUser = this.userService.findUserByUsername(userName);
			response.setUser(foundUser);
			response.setLoginStatus(true);
			response.setMessage("User with username " + userName + " successfully logged in.");
		} else {
			response.setLoginStatus(false);
			response.setMessage("User with username " + userName + " couldn't log in.");
		}
		
		return response;
		
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getUserRequest")
	@ResponsePayload
	public GetUserResponse getUser(@RequestPayload GetUserRequest request){
		
		GetUserResponse response = new GetUserResponse();
		
		Long id = Long.valueOf(request.getUserId());
		
		UserInfo found = this.userService.findUser(id); 
		
		if (found != null) {
			response.setUser(found);
		}
		
		return response;
		
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllUsersRequest")
	@ResponsePayload
	public GetAllUsersResponse getAllUsers(){
		
		GetAllUsersResponse response = new GetAllUsersResponse();
		
		List<UserInfo> users = this.userService.findAllUsers();
		
		if(users != null) {
			for(UserInfo user : users) {
				response.getUsers().add(user);
			}
		}
		
		return response;
		
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getUsersByManagerRequest")
	@ResponsePayload
	public GetUsersByManagerResponse getUsersByManager(@RequestPayload GetUsersByManagerRequest request){
		
		GetUsersByManagerResponse response = new GetUsersByManagerResponse();
		
		Long managerId = Long.valueOf(request.getManagerId());
		
		List<UserInfo> usersByManager = this.userService.findUsersByManager(managerId);
		
		if(usersByManager != null) {
			for(UserInfo user : usersByManager) {
				response.getUsers().add(user);
			}
		}
		
		return response;	
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "createUserRequest")
	@ResponsePayload
	public CreateUserResponse createUser(@RequestPayload CreateUserRequest request) throws NoSuchAlgorithmException {
		
		CreateUserResponse response = new CreateUserResponse();
		
		String firstName = request.getFirstName();
		String lastName = request.getLastName();
		String userRole = request.getUserRole();
		int managerId = request.getManagerId();
		
		response.setUser(this.userService.createUser(firstName, lastName, userRole, managerId));
		
		return response;
	}
	
}
