package org.tp.back_3tp_user_soap_ws.conversion;

import org.springframework.stereotype.Component;
import org.tp.back_3tp_user_soap_ws.domains.UserEntity;

import com.wsproject.spring.users.UserInfo;

@Component
public class UserConvertor {

	public UserInfo convertToSoapFormat(UserEntity userEntity) {
		/**
		 * Permet de convertir une entité JPA User en son équivalent soap.
		 * @param userEntity : l'équipe sous le format gérable par JPA.
		 * @return L'équipe sous le format soap.
		 */
		
		UserInfo userSoap = new UserInfo();
		
		int userId = (int)(long)userEntity.getId();
		
		userSoap.setUserId(userId); 
		userSoap.setFirstName(userEntity.getFirstName()); 
		userSoap.setLastName(userEntity.getLastName());
		userSoap.setUserName(userEntity.getUserName());
		userSoap.setManagerId(userEntity.getIdManager());
		userSoap.setUserRole(userEntity.getRole().name());
		userSoap.setActive(userEntity.isActive());
		
		return userSoap;
	}

	
	
}
