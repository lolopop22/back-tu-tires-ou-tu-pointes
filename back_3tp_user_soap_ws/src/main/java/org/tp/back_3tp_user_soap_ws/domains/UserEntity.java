package org.tp.back_3tp_user_soap_ws.domains;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity(name = "USERS") // sets the bean's name
@Table(name="USERS")
public class UserEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;	
	
	@Column(name = "first_name")
	private String firstName;
	
	
	@Column(name = "last_name")
	private String lastName;
	
	@Column(name = "user_name")
	private String userName;
	
	@Column(name = "id_manager")
	private int idManager;
	
	@Column(name = "pass")
	private String password;
	
	/*@Column(name = "is_manager")
	private boolean isManager;*/
	
	@Column(name = "role")
	@Enumerated(EnumType.STRING)
	private UserRole role;
	
	@Column(name = "active")
	private boolean active;
	
	public UserEntity() {
		
	}

	public UserEntity(String firstName, String lastName, String userName, int idManager, String role, String defaultPassword) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.userName = userName;
		this.idManager = idManager;
		this.password = defaultPassword;
		this.active = true;
		this.role = UserRole.valueOf(role);
	}
	
}
